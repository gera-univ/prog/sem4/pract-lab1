package org.spiralarms.ShapeDrawer;


import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

class Octagon implements Shape {
    private double[] xPoints;
    private double[] yPoints;

    public Octagon(double ox, double oy, double radius) {
        double sqrt2 = Math.sqrt(2);
        xPoints = new double[]{1, sqrt2 / 2, 0, -sqrt2 / 2, -1, -sqrt2 / 2, 0, sqrt2 / 2};
        yPoints = new double[]{0, sqrt2 / 2, 1, sqrt2 / 2, 0, -sqrt2 / 2, -1, -sqrt2 / 2};
        for (int i = 0; i < xPoints.length; ++i) {
            xPoints[i] *= radius;
            yPoints[i] *= radius;
            xPoints[i] += ox;
            yPoints[i] += oy;
        }
    }

    public static int[] toIntArray(double[] doubleArray) {
        final int[] intArray = new int[doubleArray.length];
        for (int i = 0; i < intArray.length; ++i)
            intArray[i] = (int) doubleArray[i];
        return intArray;
    }

    @Override
    public Polygon toPolygon() {
        return new Polygon(toIntArray(xPoints), toIntArray(yPoints), xPoints.length);
    }

    @Override
    public void applyTransform(AffineTransform at) {
        Point2D points[] = new Point2D[xPoints.length];
        for (int i = 0; i < points.length; ++i) {
            points[i] = new Point2D.Double(xPoints[i], yPoints[i]);
        }
        at.transform(points, 0, points, 0, xPoints.length);
        for (int i = 0; i < points.length; ++i) {
            xPoints[i] = points[i].getX();
            yPoints[i] = points[i].getY();
        }
    }
}