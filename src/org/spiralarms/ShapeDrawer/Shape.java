package org.spiralarms.ShapeDrawer;

import java.awt.*;
import java.awt.geom.AffineTransform;

interface Shape {
    Polygon toPolygon();

    void applyTransform(AffineTransform at);
}
