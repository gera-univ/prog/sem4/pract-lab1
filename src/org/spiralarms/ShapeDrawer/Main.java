package org.spiralarms.ShapeDrawer;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;


class GraphPanel extends JPanel {
    private Shape shape;
    private AffineTransform at;

    public GraphPanel(Shape shape, AffineTransform at) {
        this.shape = shape;
        this.at = at;
        Thread thread = new Thread() {
            public void run() {
                while (true) {
                    repaint();
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponents(g);
        g.clearRect(0, 0, getWidth(), getHeight() );
        g.setColor(Color.BLUE);
        shape.applyTransform(at);
        g.drawPolygon(shape.toPolygon());
    }
}


public class Main {

    static final int WW = 640;
    static final int WH = 480;

    public static void printHelp(int exitStatus) {
        System.out.printf("Draws an octagon rotating around specified point\n\nUsage: java %s originX originY radius anchorX anchorY angle\n\nExample: java %s 300 400 100 300 300 -0.05\n", Main.class.getCanonicalName(),Main.class.getCanonicalName());
        System.exit(exitStatus);
    }
    public static void main(String[] arguments) {
        if (arguments.length != 6)
            printHelp(0);

        double ox = 0;
        double oy = 0;
        double radius = 0;
        double ax = 0;
        double ay = 0;
        double theta = 0;
        try {
            ox = Double.parseDouble(arguments[0]);
            oy = Double.parseDouble(arguments[1]);
            radius = Double.parseDouble(arguments[2]);
            ax = Double.parseDouble(arguments[3]);
            ay = Double.parseDouble(arguments[4]);
            theta = Double.parseDouble(arguments[5]);
        } catch (NumberFormatException e) {
            printHelp(1);
        }

        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame f = new JFrame("Rotating octagon");
        f.setSize(WW, WH);
        f.setResizable(false);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Octagon o = new Octagon(ox, oy, radius);
        AffineTransform at = AffineTransform.getRotateInstance(theta, ax, ay);
        GraphPanel gp = new GraphPanel(o, at);

        f.setContentPane(gp);

        f.setVisible(true);
    }
}